#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar

try:
    METHOD = sys.argv[1]
    USER = sys.argv[2]
    ELEMENTS = USER.split("@")
    USERNAME = ELEMENTS[0]
    IPPORT = ELEMENTS[1].split(":")
    IP = IPPORT[0]
    PORT = int(IPPORT[1])

    if METHOD not in ("INVITE", "BYE"):
        sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")

except IndexError:
    sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
except ValueError:
    sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            Request = METHOD.upper() + " sip:" + USERNAME + "@" + IP + " SIP/2.0\r\n"

            # Cabecera
            header = "application/sdp"
            header2 = "66"

            # Definimos los parámetros en formato SDP
            version = "0"
            origin = "robin@gotham.com 127.0.0.1"
            sessionname = "audiosession"
            time = "0"
            rtp_port = "34543"
            media = "audio " + rtp_port + " RTP"

            # Construimos la petición INVITE con el SDP
            if METHOD.upper() == "INVITE":

                Invite = f"{Request}Content-type:{header}\r\nContent-lenght:{header2}\r\n\r\nv={version}\r\no={origin}\r\ns={sessionname}\r\nt={time}\r\nm={media}\r\n"
                print(Invite)
                # Mandamos la petición
                my_socket.sendto(Invite.encode('utf-8'), (IP, PORT))
            elif METHOD.upper() == "BYE":
                my_socket.sendto(Request.encode('utf-8'), (IP, PORT))

            Response1 = my_socket.recv(1024).decode('utf-8')
            Response2 = Response1.splitlines()
            print(Response1)

            if Response2[0] == "SIP/2.0 200 OK":
                if METHOD == "INVITE":
                    ack = f"ACK sip:{USERNAME}@{IP} SIP/2.0\r\n"
                    print("Sending ACK")
                    my_socket.sendto(ack.encode('utf-8'), (IP, PORT))
            else:
                print("An error has ocurred")

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
