#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socketserver
import sys
import simplertp
import random

try:
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    audio_file = sys.argv[3]
    file = open(audio_file)
    file.close()
except IndexError:
    print("Usage: python3 server.py <IP> <port> <audio_file>")
except ValueError:
    print("Usage: python3 server.py <IP> <port> <audio_file>")
except FileNotFoundError:
    print("Usage: python3 server.py <IP> <port> <audio_file>")


class SIPRequest:

    def __init__(self, data):
        self.data = data.decode('utf-8')

    def parse(self):
        self._parse_command(self.data)
        headers = self.data.splitlines()[4:]
        self._parse_headers(headers)

    def _get_address(self, uri):
        self.uri = uri
        listuri = self.uri.split(":")
        schema = listuri[0]
        address = listuri[1]
        return address, schema

    def _parse_command(self, line):
        listdata = line.split()
        self.command = listdata[0].upper()
        self.uri = listdata[1]
        uri = self._get_address(self.uri)
        self.address = uri[0]
        usernameIP = self.address.split("@")
        self.username = usernameIP[0]
        self.IP = usernameIP[1]
        schema = uri[1]

        if (self.command == "INVITE" or self.command == "ACK" or self.command == "BYE") and schema == "sip":
            self.result = "200 OK"
        elif self.command != "INVITE" or self.command != "ACK" or self.command != "BYE":
            self.result = "405 Method not allowed"
        elif schema != "sip":
            self.result = "400 Bad format"

    def _parse_headers(self, first_nl):
        self.headers = []
        if len(first_nl) > 0:
            self.version = first_nl[0]
            originclient = first_nl[1]
            clientIP = originclient.split()[1]
            self.headers.append(clientIP)
            self.sessionname = first_nl[2]
            self.time = first_nl[3]
            clientmedia = first_nl[4]
            clientrtpport = clientmedia.split()[1]
            self.headers.append(clientrtpport)


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    Client_IPPort = []

    def process_register(self, sip_request):

        self.Client_IPPort.append(sip_request.headers[0])
        self.Client_IPPort.append(sip_request.headers[1])

    def handle(self):

        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]

        print("Received: " + data.decode("utf-8") + f"from {self.client_address[0]}\r\n")

        sip_request = SIPRequest(data)
        sip_request.parse()

        if (sip_request.command == "ACK") and (sip_request.result == "200 OK"):

            ALEAT = random.randint(0, 10000)

            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio, self.Client_IPPort[0], int(self.Client_IPPort[1]))

        elif (sip_request.command == "INVITE") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)

            # Definimos los parámetros de el SDP que cambian con respecto a la petición
            origin = f"{sip_request.username}@gotham.com {sip_request.IP}"
            media = f"audio 67876 RTP"

            response = f"SIP/2.0 {sip_request.result}\r\n\r\n{sip_request.version}\r\no={origin}\r\n{sip_request.sessionname}\r\n{sip_request.time}\r\nm={media}\r\n"
            print(f"Sent: {response}")
            sock.sendto(response.encode(), self.client_address)
        else:
            sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    # Listens at port (my address)
    # and calls the SIPRegisterHandlers class to manage the request
    try:
        serv = socketserver.UDPServer((IP, PORT), SIPRegisterHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
